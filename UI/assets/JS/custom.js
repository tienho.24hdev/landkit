const openHeader = document.querySelector('.open-modal')
const closeHeader = document.querySelector('.close-modal')
const modal = document.querySelector('.modal')
const main = document.querySelector('.main')
const openSubNav = document.querySelectorAll('.open-subnav')
const animates = document.querySelectorAll('.animated')
const text_change = document.querySelector('.text-change')
const numsIncreaseWrap = document.querySelector('.stat')
const numsIncrease = document.querySelectorAll('.increase-nums')
const switchBtn = document.querySelector('.switch-btn input')
const switchNum = document.querySelector('.price .number')
const slides = document.querySelectorAll('.wrap .slide .card')
const inputName = document.querySelector('input[type=text]')
const inputEmail = document.querySelector('input[type=email]')
const inputPassword = document.querySelector('input[type=password]')
const errName = document.querySelector('.errName')
const errEmail= document.querySelector('.errEmail')
const errPassword = document.querySelector('.errPassword')
const submitForm = document.querySelector('.input-form .button')

let isNotCounted = true;
let slideIndex = 1;

// Open, close header
openHeader.addEventListener('click', () => {
    modal.style.display = 'block';
    document.body.style.overflowY = 'hidden';
    main.style.overflowY = 'hidden';
    modal.style.opacity = '1';
    modal.style.transform = "scale(1)"

})
closeHeader.addEventListener('click', () => {
    modal.style.display = 'none';
    document.body.style.overflowY = 'auto';
    main.style.overflowY = 'auto';
    modal.style.opacity = '0';
    modal.style.transform = "scale(0)"

})
openSubNav.forEach(item => {
    item.addEventListener('click', () => {
        const sub_nav_list = item.querySelector('.sub-nav-list');
        sub_nav_list.classList.toggle('show')
        item.classList.toggle('rotate')
    })
})

// Animations: FadeIn, FadeLeft, FadeRight
window.addEventListener('scroll', () => {
    animates.forEach(animation => {
        const top = animation.getBoundingClientRect().top
        const bottom = animation.getBoundingClientRect().bottom
        if (top < this.window.innerHeight && bottom >= 0) {
            if (animation.getAttribute('data-animate-name') != null) {
                animation.classList.add(animation.getAttribute('data-animate-name'))
            }
        }
    })
    if (isNotCounted) {
        numsIncrease.forEach(num => {
            const top = num.getBoundingClientRect().top
            const bottom = num.getBoundingClientRect().bottom
            if (top < this.window.innerHeight && bottom >= 0) {
                countTo(num, 0, num.textContent);
                isNotCounted = false;
            }
        })
    }
})

// Animation: Typing
const textType = () => {
    setTimeout(() => {
        text_change.textContent = "developers.";
    }, 0);
    setTimeout(() => {
        text_change.textContent = "founders.";
    }, 2000);
    setTimeout(() => {
        text_change.textContent = "designers.";
    }, 4000);
}
textType()
setInterval(() => {
    textType()
}, 6000);

// Animation: Number Counting
const countTo = (qSeclector, start = 0, end, duration = 10) => {
    let step = end > start ? 1 : -1;

    if (start == end) {
        qSeclector.textContent = end;
        return;
    }
    let counter = setInterval(function () {
        start += step;
        qSeclector.textContent = start;

        if (start == end) {
            clearInterval(counter);
        }
    }, duration);
}

// Animation: Switching Number
switchBtn.addEventListener('change', () => {
    if (switchBtn.checked) {
        countTo(switchNum, 29, 49, 50)
    }
    else {
        countTo(switchNum, 49, 29, 50)
    }
})

// SlideShow Animation
const nextSlides = (n) => {
    showSlides(slideIndex += n);
}
const prevSlides = (n) => {
    slideIndex += n
    n = slideIndex
    if (n > slides.length) { slideIndex = 1 }
    if (n < 1) { slideIndex = slides.length }
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.transform = "translateX(-100%)"
        slides[i].querySelector('.img .img').classList.remove('slideImg')
        slides[i].querySelector('.card-body').style.transform = "translateX(-100%)"
    }
    slides[slideIndex - 1].style.transform = "translateX(0)"
    slides[slideIndex - 1].querySelector('.img .img').classList.add('slideImg')
    slides[slideIndex - 1].querySelector('.card-body').style.transform = "translateX(0)"
}
const showSlides = (n) => {
    if (n > slides.length) { 
        slideIndex = 1
    }
    if (n < 1) { 
        slideIndex = slides.length 
    }
    for (let i = 0; i < slides.length; i++) {
        slides[i].style.transform = "translateX(100%)"
        slides[i].querySelector('.img .img').classList.remove('slideImg')
        slides[i].querySelector('.card-body').style.transform = "translateX(100%)"
    }
    slides[slideIndex - 1].style.transform = "translateX(0)"
    slides[slideIndex - 1].querySelector('.img .img').classList.add('slideImg')
    slides[slideIndex - 1].querySelector('.card-body').style.transform = "translateX(0)"
}
showSlides(slideIndex);

// Validate inputs
const isEmpty = (value) => {
    if(value) return true
    else return false
}
const isValidEmail = (email) => {
    let regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/
    if(!regex.test(email)){
        return false
    }
    return true
}
const minLength = (str, minlength) => {
    if(str.length < minlength) return false
    return true 
}
const validateInput = () => {
    if(!isEmpty(inputName.value)) {
        errName.innerHTML = "Please enter your name!"
    }
    else {
        errName.innerHTML = ""
    }
    if(!isEmpty(inputEmail.value) || !isValidEmail(inputEmail.value)) {
        errEmail.innerHTML = "Invalid email!"
    }
    else {
        errEmail.innerHTML = ""
    }
    if(!isEmpty(inputPassword.value) || !minLength(inputPassword.value, 8)) {
        errPassword.innerHTML = "Invalid password!"
    }
    else {
        errPassword.innerHTML = ""
    }
}
inputName.addEventListener('keypress', (e) => {
    if(e.key === "Enter") {
        e.preventDefault();
        if(!isEmpty(inputName.value)) {
            errName.innerHTML = "Please enter your name!"
        }
        else {
            errName.innerHTML = ""
        }
    }
})
inputEmail.addEventListener('keypress', (e) => {
    if(e.key === "Enter") {
        e.preventDefault();
        if(!isEmpty(inputEmail.value) || !isValidEmail(inputName.value)) {
            errEmail.innerHTML = "Invalid email!"
        }
        else {
            errEmail.innerHTML = ""
        }
    }
})
inputPassword.addEventListener('keypress', (e) => {
    if(e.key === "Enter") {
        e.preventDefault();
        if(!isEmpty(inputPassword.value) || !minLength(inputPassword.value, 8)) {
            errPassword.innerHTML = "Invalid password!"
        }
        else {
            errEmail.innerHTML = ""
        }
    }
})
submitForm.addEventListener('click', (e) => {
    e.preventDefault();
    validateInput()
})