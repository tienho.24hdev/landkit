const employeeList = document.querySelector('.employee-list')
const prev_btn = document.getElementById('prev_link')
const next_btn = document.getElementById('next_link')
const number_page = document.querySelector('.number-page')
const selectItem = document.querySelector('.select-list')
const fromNum = document.querySelector('.from')
const toNum = document.querySelector('.to')
const entries = document.querySelector('.entries')
const input = document.querySelector('.search')
const noti = document.querySelector('.noti')
const sortBtns = document.querySelectorAll('.sorting')
let list_Employee = []
let resSearch = []
let resSort = []

const url = "https://63f1f956f28929a9df512124.mockapi.io/employees"

let perPage = 10
let currentPage = 1
let start = 0
let end = perPage
let totalPages = 0
fromNum.innerHTML = start + 1
toNum.innerHTML = end

function main() {
    getData(renderData)
}
function getData(callback) {
    fetch(url)
        .then(function (response) {
            // console.log(response);
            return response.json()
        })
        .then(function (response) {
            list_Employee = response
            totalPages = Math.ceil(list_Employee.length / perPage)
            entries.innerHTML = list_Employee.length
            renderListPage(totalPages, list_Employee)
            return response
        })
        .then(callback);
}
function renderData(list_Employee) {
    let htmls = ''
    const content = list_Employee.map((emp, index) => {
        if (index >= start && index < end) {
            htmls += `<tr class="h-[50px] odd:bg-[#F6F6F6] even:bg-white">`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">${emp.name}</td>`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">${emp.position}</td>`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">${emp.office}</td>`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">${emp.age}</td>`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">${emp.startdate}</td>`
            htmls += `<td class="border-y border-y-[#D9D9D9] pl-2">$${emp.salary}</td>`
            htmls += `</tr>`
            return htmls
        }
    })
    employeeList.innerHTML = htmls
}
function changePage(list_Employee) {
    const currentNumberPage = document.querySelectorAll('.number-page div')
    for (let i = 0; i < currentNumberPage.length; i++) {
        // console.log(currentNumberPage[i]);
        currentNumberPage[i].addEventListener('click', () => {
            let value = i + 1
            currentPage = value
            $('.number-page div').removeClass('from-[#e6e6e61a] to-[#0000001a]')
            $('.number-page div').removeClass('border')
            $('.number-page div').removeClass('border-[#AFAFAF]')
            currentNumberPage[i].classList.add('border')
            currentNumberPage[i].classList.add('border-[#AFAFAF]')
            currentNumberPage[i].classList.add('bg-gradient-to-b')
            currentNumberPage[i].classList.add('from-[#e6e6e61a]')
            currentNumberPage[i].classList.add('to-[#0000001a]')
            // console.log(currentNumberPage[i]);
            start = (currentPage - 1) * perPage
            end = currentPage * perPage
            fromNum.innerHTML = start + 1
            toNum.innerHTML = end
            // console.log(currentPage);
            renderData(list_Employee)
            next_btn.classList.remove('text-[#666]')
            next_btn.classList.add('cursor-pointer')
            next_btn.classList.add('from-[#585858]')
            next_btn.classList.add('hover:text-white')
            next_btn.classList.add('to-[#111]')
            prev_btn.classList.remove('text-[#666]')
            prev_btn.classList.add('cursor-pointer')
            prev_btn.classList.add('from-[#585858]')
            prev_btn.classList.add('hover:text-white')
            prev_btn.classList.add('to-[#111]')
            if (i == 0) {
                prev_btn.classList.add('text-[#666]')
                prev_btn.classList.remove('cursor-pointer')
                prev_btn.classList.remove('to-[#111]')
                prev_btn.classList.remove('from-[#585858]')
                prev_btn.classList.remove('hover:text-white')
            }
            if (i == currentNumberPage.length - 1) {
                next_btn.classList.add('text-[#666]')
                next_btn.classList.remove('cursor-pointer')
                next_btn.classList.remove('to-[#111]')
                next_btn.classList.remove('from-[#585858]')
                next_btn.classList.remove('hover:text-white')
                toNum.innerHTML = list_Employee.length
            }
        })
    }
}
function renderListPage(totalPages, list_Employee) {
    let htmls = ''
    htmls += `<div class="float-left py-2 px-4 duration-300 bg-gradient-to-b from-[#e6e6e61a] to-[#0000001a] border border-[#AFAFAF] mx-1 cursor-pointer hover:text-white hover:border-[#111] hover:from-[#585858] hover:to-[#111]">${1}</div>`
    for (let i = 2; i <= totalPages; i++) {
        htmls += `<div class=" float-left py-2 px-4 duration-300 mx-1 cursor-pointer bg-gradient-to-b hover:text-white hover:border-[#111] hover:from-[#585858] hover:to-[#111]">${i}</div>`
    }
    number_page.innerHTML = htmls
    changePage(list_Employee)
}
next_btn.addEventListener('click', () => {
    currentPage++
    if (currentPage > totalPages) {
        currentPage = totalPages
    }
    if (currentPage === totalPages) {
        next_btn.classList.add('text-[#666]')
        next_btn.classList.remove('cursor-pointer')
        next_btn.classList.remove('from-[#585858]')
        next_btn.classList.remove('hover:text-white')
        next_btn.classList.remove('to-[#111]')
    }
    prev_btn.classList.remove('text-[#666]')
    prev_btn.classList.add('cursor-pointer')
    prev_btn.classList.add('to-[#111]')
    prev_btn.classList.add('from-[#585858]')
    prev_btn.classList.add('hover:text-white')
    $('.number-page div').removeClass('from-[#e6e6e61a] to-[#0000001a]')
    $('.number-page div').removeClass('border')
    $('.number-page div').removeClass('border-[#AFAFAF]')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('from-[#e6e6e61a] to-[#0000001a]')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('border')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('border-[#AFAFAF]')
    start = (currentPage - 1) * perPage
    end = currentPage * perPage
    fromNum.innerHTML = start + 1
    if (currentPage === totalPages) {
        toNum.innerHTML = list_Employee.length
    }
    else {
        toNum.innerHTML = end
    }
    renderData(list_Employee)
})
prev_btn.addEventListener('click', () => {
    currentPage--
    if (currentPage < 1) {
        currentPage = 1
    }
    if (currentPage === 1) {
        prev_btn.classList.add('text-[#666]')
        prev_btn.classList.remove('cursor-pointer')
        prev_btn.classList.remove('to-[#111]')
        prev_btn.classList.remove('from-[#585858]')
        prev_btn.classList.remove('hover:text-white')
    }
    next_btn.classList.remove('text-[#666]')
    next_btn.classList.add('cursor-pointer')
    next_btn.classList.add('from-[#585858]')
    next_btn.classList.add('hover:text-white')
    next_btn.classList.add('to-[#111]')
    $('.number-page div').removeClass('from-[#e6e6e61a] to-[#0000001a]')
    $('.number-page div').removeClass('border')
    $('.number-page div').removeClass('border-[#AFAFAF]')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('from-[#e6e6e61a] to-[#0000001a]')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('border')
    $(`.number-page div:eq(${currentPage - 1})`).addClass('border-[#AFAFAF]')
    start = (currentPage - 1) * perPage
    end = currentPage * perPage
    fromNum.innerHTML = start + 1
    if (currentPage === totalPages) {
        toNum.innerHTML = list_Employee.length
    }
    else {
        toNum.innerHTML = end
    }
    renderData(list_Employee)
})
selectItem.addEventListener('change', () => {
    searchEmployee(list_Employee)
    renderFromTo(searchEmployee(list_Employee))
    renderListPage(totalPages, searchEmployee(list_Employee))
    renderData(searchEmployee(list_Employee))
})
function renderFromTo(list_Employee) {
    const select = selectItem.options[selectItem.selectedIndex].innerHTML
    perPage = select
    totalPages = Math.ceil(list_Employee.length / perPage)
    start = 0
    end = perPage
    fromNum.innerHTML = start + 1
    entries.innerHTML = list_Employee.length
    if (select > list_Employee.length) {
        toNum.innerHTML = list_Employee.length
    }
    else {
        toNum.innerHTML = end
    }
}
function searchEmployee(list_Employee) {
    if (input.value != "") {
        resSearch = list_Employee.filter(emp => {
            for (let x of Object.values(emp)) {
                if (x.toString().toLowerCase().includes(input.value.toLowerCase())) {
                    return emp
                }
            }
        });
        noti.innerHTML = ` (filtered from ${list_Employee.length} entries)`
    }
    else {
        resSearch = list_Employee
        noti.innerHTML = ''
    }
    renderFromTo(resSearch)
    renderListPage(totalPages, resSearch)
    renderData(resSearch)
}
input.addEventListener('input', () => {
    searchEmployee(list_Employee)
})
function sortAsc(property) {
    if (resSearch.length == 0) {
        resSort = list_Employee.sort((a, b) => {
            if (a[property] < b[property]) {
                return -1
            }
            if (a[property] > b[property]) {
                return 1
            }
            return 0
        })
    }
    else {
        resSort = resSearch.sort((a, b) => {
            if (a[property] < b[property]) {
                return -1
            }
            if (a[property] > b[property]) {
                return 1
            }
            return 0
        })
    }
    renderFromTo(resSort)
    renderListPage(totalPages, resSort)
    renderData(resSort)
}
function sortDesc(property) {
    if (resSearch.length == 0) {
        resSort = list_Employee.sort((a,b) => {
            if (a[property] < b[property]) {
                return 1
            }
            if (a[property] > b[property]) {
                return -1
            }
            return 0
        })
    }
    else {
        resSort = resSearch.sort((a,b) => {
            if (a[property] < b[property]) {
                return 1
            }
            if (a[property] > b[property]) {
                return -1
            }
            return 0
        })
    }
    renderFromTo(resSort)
    renderListPage(totalPages, resSort)
    renderData(resSort)
}
sortBtns.forEach(btn => {
    btn.addEventListener('click', (e) => {
        e.preventDefault();
        sortBtns.forEach(item => {
            if (item !== btn) {
                item.classList.remove('sorting-asc')
                item.classList.remove('sorting-desc')
            }
        })
        if (btn.classList.contains('sorting-desc')) {
            btn.classList.add('sorting-asc');
            btn.classList.remove('sorting-desc');
            sortDesc(btn.innerHTML.toLowerCase().split(" ").join(""));
        } else {
            btn.classList.remove('sorting-asc');
            btn.classList.add('sorting-desc');
            sortAsc(btn.innerHTML.toLowerCase().split(" ").join(""));
        }
    })
})

main()
